#ifndef OPENCL_UTIL_H_
#define OPENCL_UTIL_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/**
 * \brief       Print info on OpenCL-enabled platforms.
 * \param out   Where to print the result. Use 'stdout' (without the quotes) to print to console.
 * \return      Number of platforms found.
 */
size_t cl_print_dev_info(FILE *out);

#endif /* OPENCL_UTIL_H_ */

