cmake_minimum_required(VERSION 3.1) # For FindOpenCL
project(devcl)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY build)

find_package(OpenCL REQUIRED)

include_directories(include ${OpenCL_INCLUDE_DIR})

add_executable(devcl src/main.c src/utils.c)

target_link_libraries(devcl ${OpenCL_LIBRARIES})

