opencl-tmplt
============
To generate the build files for your system:

    $ cmake .

It's possible to get project files for many IDEs and toolchains.

You can add a path if cmake has some trouble finding opencl (generally because
your env variables need some love):

    $ cmake -DCMAKE_PREFIX_PATH=/some/path/ .

...and to get optimization/debugging flags either

    $ cmake -DCMAKE_BUILD_TYPE=RELEASE .

or

    $ cmake -DCMAKE_BUILD_TYPE=DEBUG .

On UNIX, after cmake made its magic, building is simply:

    $ make

