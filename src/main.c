#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "utils.h"

int main(int argc, char **argv) {
  printf("Querying info on available platforms and devices...\nPlatforms:");
  cl_print_dev_info(stdout);
  return EXIT_SUCCESS;
}

