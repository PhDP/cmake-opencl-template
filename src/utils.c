#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#if defined(__APPLE__) && defined(__MACH__)
  #include <OpenCL/OpenCL.h>
#else
  #include <CL/cl.h>
#endif

size_t cl_print_dev_info(FILE *out) {
  char* info;
  size_t infoSize;
  cl_uint num_platform;
  cl_platform_id *platforms;
  const char* attributeNames[5] = { "Name", "Vendor", "Version", "Profile", "Extensions" };
  const cl_platform_info attributeTypes[5] = { CL_PLATFORM_NAME, CL_PLATFORM_VENDOR, CL_PLATFORM_VERSION, CL_PLATFORM_PROFILE, CL_PLATFORM_EXTENSIONS };
  const int attributeCount = sizeof(attributeNames) / sizeof(char*);
  clGetPlatformIDs(5, NULL, &num_platform);
  platforms = (cl_platform_id*)malloc(sizeof(cl_platform_id) * num_platform);
  clGetPlatformIDs(num_platform, platforms, NULL);
  int i = 0;
  for (; i < num_platform; ++i) {
    fprintf(out, "\n %d.\n", i);
    int j = 0;
    for (; j < attributeCount; ++j) {
      // get platform attribute value size
      clGetPlatformInfo(platforms[i], attributeTypes[j], 0, NULL, &infoSize);
      info = (char*)malloc(infoSize);
      // get platform attribute value
      clGetPlatformInfo(platforms[i], attributeTypes[j], infoSize, info, NULL);
      fprintf(out, "  %d.%d %-11s: %s\n", i, j + 1, attributeNames[j], info);
      free(info);
    }
    printf("\n");
  }
  free(platforms);
  return (size_t)num_platform;
}

